-- Copyright (C) 2022 Stephan Kreutzer
--
-- This file is part of progress tracker server.
--
-- progress tracker server is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License version 3 or any later version,
-- as published by the Free Software Foundation.
--
-- progress tracker server is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU Affero General Public License 3 for more details.
--
-- You should have received a copy of the GNU Affero General Public License 3
-- along with progress tracker server. If not, see <http://www.gnu.org/licenses/>.



CREATE DATABASE `progress_tracker` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE progress_tracker;

CREATE USER 'progress_tracker_user'@'localhost' IDENTIFIED BY 'password';
GRANT USAGE ON *.* TO 'progress_tracker_user'@'localhost' IDENTIFIED BY 'password' WITH MAX_QUERIES_PER_HOUR 0
  MAX_CONNECTIONS_PER_HOUR 0
  MAX_UPDATES_PER_HOUR 0
  MAX_USER_CONNECTIONS 0;
GRANT ALL PRIVILEGES ON `progress_tracker`.* TO 'progress_tracker_user'@'localhost';


CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE `person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) COLLATE utf8_bin NOT NULL,
  `name_family` VARCHAR(255) COLLATE utf8_bin,
  `name_given` VARCHAR(255) COLLATE utf8_bin,
  `name_alternate` VARCHAR(255) COLLATE utf8_bin,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
