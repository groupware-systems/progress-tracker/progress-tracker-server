<?php
/* Copyright (C) 2022 Stephan Kreutzer
 *
 * This file is part of progress tracker server.
 *
 * progress tracker server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * progress tracker server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with progress tracker server. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/api/person.php
 * @author Stephan Kreutzer
 * @since 2022-09-13
 */



require_once("./libraries/database.inc.php");

if ($_SERVER['REQUEST_METHOD'] === "GET")
{
    $persons = Database::Get()->QueryUnsecure("SELECT `id`,\n".
                                              "    `name`,\n".
                                              "    `name_family`,\n".
                                              "    `name_given`,\n".
                                              "    `name_alternate`\n".
                                              "FROM `".Database::Get()->GetPrefix()."person`\n".
                                              "WHERE 1\n".
                                              "ORDER BY `id` ASC");

    if (is_array($persons) !== true)
    {
        http_response_code(500);
        exit(-1);
    }

    header("Content-Type: application/json");

    /** @todo This may provide only links to the individual persons instead of
      * the full data of each project, and provide a person's data if GET
      * requested with its ID. */
    echo "{\"person\":[";

    for ($i = 0, $max = count($persons); $i < $max; $i++)
    {
        if ($i > 0)
        {
            echo ",";
        }

        echo "{\"id\":".$persons[$i]['id'].",".
             "\"name\":".json_encode($persons[$i]['name']);

        if ($persons[$i]['name_family'] !== NULL)
        {
            echo ",\"name-family\":".json_encode($persons[$i]['name_family']);
        }

        if ($persons[$i]['name_given'] !== NULL)
        {
            echo ",\"name-given\":".json_encode($persons[$i]['name_given']);
        }

        if ($persons[$i]['name_alternate'] !== NULL)
        {
            echo ",\"name-alternate\":".json_encode($persons[$i]['name_alternate']);
        }

        echo "}";
    }

    echo "]}";
}
else if ($_SERVER['REQUEST_METHOD'] === "POST")
{
    $payload = "";

    {
        $source = @fopen("php://input", "r");

        while (true)
        {
            $chunk = @fread($source, 1024);

            if ($chunk == false)
            {
                break;
            }

            $payload .= $chunk;
        }
    }

    $payload = json_decode($payload, true);

    if ($payload === false)
    {
        http_response_code(400);
        exit(1);
    }

    if (is_array($payload) != true)
    {
        http_response_code(400);
        exit(1);
    }

    if (isset($payload['name']) != true)
    {
        http_response_code(400);
        echo "'name' is missing.";
        exit(1);
    }

    if (strlen($payload['name']) <= 0)
    {
        http_response_code(400);
        echo "'name' is an empty string.";
        exit(1);
    }

    $arrayValues = array();
    $arrayTypes = array();

    $arrayValues[0] = NULL;
    $arrayValues[1] = $payload['name'];
    $arrayValues[2] = NULL;
    $arrayValues[3] = NULL;
    $arrayValues[4] = NULL;

    $arrayTypes[0] = Database::TYPE_NULL;
    $arrayTypes[1] = Database::TYPE_STRING;
    $arrayTypes[2] = Database::TYPE_NULL;
    $arrayTypes[3] = Database::TYPE_NULL;
    $arrayTypes[4] = Database::TYPE_NULL;

    if (isset($payload['name-family']) === true)
    {
        if (strlen($payload['name-family']) > 0)
        {
            $arrayValues[2] = $payload['name-family'];
            $arrayTypes[2] = Database::TYPE_STRING;
        }
    }

    if (isset($payload['name-given']) === true)
    {
        if (strlen($payload['name-given']) > 0)
        {
            $arrayValues[3] = $payload['name-given'];
            $arrayTypes[3] = Database::TYPE_STRING;
        }
    }

    if (isset($payload['name-alternate']) === true)
    {
        if (strlen($payload['name-alternate']) > 0)
        {
            $arrayValues[4] = $payload['name-alternate'];
            $arrayTypes[4] = Database::TYPE_STRING;
        }
    }

    $id = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."person` (`id`,\n".
                                  "    `name`,\n".
                                  "    `name_family`,\n".
                                  "    `name_given`,\n".
                                  "    `name_alternate`)\n".
                                  "VALUES (?, ?, ?, ?, ?)\n",
                                  $arrayValues,
                                  $arrayTypes);

    if ($id <= 0)
    {
        http_response_code(500);
        exit(-1);
    }

    $person = Database::Get()->Query("SELECT `id`,\n".
                                     "    `name`,\n".
                                     "    `name_family`,\n".
                                     "    `name_given`,\n".
                                     "    `name_alternate`\n".
                                     "FROM `".Database::Get()->GetPrefix()."person`\n".
                                     "WHERE `id`=?",
                                     array($id),
                                     array(Database::TYPE_INT));

    if (is_array($person) !== true)
    {
        http_response_code(500);
        exit(-1);
    }

    if (count($person) <= 0)
    {
        http_response_code(500);
        exit(-1);
    }

    $person = $person[0];

    http_response_code(201);
    header("Content-Type: application/json");

    echo "{\"id\":".$person['id'].",".
         "\"name\":".json_encode($person['name']);

    if ($person['name_family'] !== NULL)
    {
        echo ",\"name-family\":".json_encode($person['name_family']);
    }

    if ($person['name_given'] !== NULL)
    {
        echo ",\"name-given\":".json_encode($person['name_given']);
    }

    if ($person['name_alternate'] !== NULL)
    {
        echo ",\"name-alternate\":".json_encode($person['name_alternate']);
    }

    echo "}";
}
else
{
    http_response_code(405);
    exit(1);
}



?>
