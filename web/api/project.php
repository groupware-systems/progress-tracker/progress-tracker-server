<?php
/* Copyright (C) 2022 Stephan Kreutzer
 *
 * This file is part of progress tracker server.
 *
 * progress tracker server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * progress tracker server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with progress tracker server. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/web/api/project.php
 * @author Stephan Kreutzer
 * @since 2022-09-08
 */



require_once("./libraries/database.inc.php");

if ($_SERVER['REQUEST_METHOD'] === "GET")
{
    $projects = Database::Get()->QueryUnsecure("SELECT `id`,\n".
                                               "    `title`\n".
                                               "FROM `".Database::Get()->GetPrefix()."project`\n".
                                               "WHERE 1\n".
                                               "ORDER BY `id` ASC");

    if (is_array($projects) !== true)
    {
        http_response_code(500);
        exit(-1);
    }

    header("Content-Type: application/json");

    /** @todo This may provide only links to the individual projects instead of
      * the full data of each project, and provide a project's data if GET
      * requested with its ID. */
    echo "{\"project\":[";

    for ($i = 0, $max = count($projects); $i < $max; $i++)
    {
        if ($i > 0)
        {
            echo ",";
        }

        echo "{\"id\":".$projects[$i]['id'].",\"title\":".json_encode($projects[$i]['title'])."}";
    }

    echo "]}";
}
else if ($_SERVER['REQUEST_METHOD'] === "POST")
{
    $payload = "";

    {
        $source = @fopen("php://input", "r");

        while (true)
        {
            $chunk = @fread($source, 1024);

            if ($chunk == false)
            {
                break;
            }

            $payload .= $chunk;
        }
    }

    $payload = json_decode($payload, true);

    if ($payload === false)
    {
        http_response_code(400);
        exit(1);
    }

    if (is_array($payload) != true)
    {
        http_response_code(400);
        exit(1);
    }

    if (isset($payload['title']) != true)
    {
        http_response_code(400);
        echo "'title' is missing.";
        exit(1);
    }

    $id = Database::Get()->Insert("INSERT INTO `".Database::Get()->GetPrefix()."project` (`id`,\n".
                                  "    `title`)\n".
                                  "VALUES (?, ?)\n",
                                  array(NULL, $payload['title']),
                                  array(Database::TYPE_NULL, Database::TYPE_STRING));

    if ($id <= 0)
    {
        http_response_code(500);
        exit(-1);
    }

    $project = Database::Get()->Query("SELECT `id`,\n".
                                      "    `title`\n".
                                      "FROM `".Database::Get()->GetPrefix()."project`\n".
                                      "WHERE `id`=?",
                                      array($id),
                                      array(Database::TYPE_INT));

    if (is_array($project) !== true)
    {
        http_response_code(500);
        exit(-1);
    }

    if (count($project) <= 0)
    {
        http_response_code(500);
        exit(-1);
    }

    $project = $project[0];

    http_response_code(201);
    header("Content-Type: application/json");

    echo "{\"id\":".$project['id'].",\"title\":".json_encode($project['title'])."}";
}
else
{
    http_response_code(405);
    exit(1);
}



?>
