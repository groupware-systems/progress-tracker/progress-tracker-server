<?php
$pdo = false;
$db_table_prefix = ""; // Prefix for database tables.
$exceptionConnectFailure = NULL;


try
{
    $pdo = @new PDO('mysql:host=localhost;dbname=progress_tracker;charset=utf8', "progress_tracker_user", "password", array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
}
catch (PDOException $ex)
{
    $pdo = false;
    $exceptionConnectFailure = $ex;
}

?>
